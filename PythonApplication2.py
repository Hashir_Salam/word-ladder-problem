import json
from pprint import pprint
from pythonds.graphs import Graph
from pythonds.basic import Queue

#ACID to BONE
#GRAPH to HEEDY

with open('dict.json') as data_file:    
    data = json.load(data_file)         #Loading the Json File


keys = []
for k in data:
    keys.append(k)                     #Adding the words to a list 
    #print k

keys.sort()                            #Sorting the list in Aplphabatical order 
#for k in keys:
#    print k

#Task: create buckets of words that differ by one letter


def buildGraph(keys,length):
    d = {}
    g = Graph()
   
    #wfile = open(wordFile,'r')
    # create buckets of words that differ by one letter
    for k in keys:
        if(k.find('-') ==-1 and len(k) == length):      #ONLY 4 LETTER   
            #word = k[:-1]
            word = k
            #print word          
            for i in range(len(word)):
                bucket = word[:i] + '_' + word[i+1:]
                if bucket in d:
                    d[bucket].append(word)
                else:
                    d[bucket] = [word]
    
    #for p in d['WEA_']:  Prints bucket 
    #    print p
    # add vertices and edges for words in the same bucket
    for bucket in d.keys():
        for word1 in d[bucket]:
            for word2 in d[bucket]:
                if word1 != word2:
                    g.addEdge(word1,word2)
    
    return g

def bfs(g,start):
  start.setDistance(0)
  start.setPred(None)
  vertQueue = Queue()
  vertQueue.enqueue(start)
  while (vertQueue.size() > 0):
    currentVert = vertQueue.dequeue()
    for nbr in currentVert.getConnections():
      if (nbr.getColor() == 'white'):
        nbr.setColor('gray')
        nbr.setDistance(currentVert.getDistance() + 1)
        nbr.setPred(currentVert)
        vertQueue.enqueue(nbr)
    currentVert.setColor('black')


def traverse(y):
    x = y
    while (x.getPred()):
        print(x.getId())
        x = x.getPred()
    print(x.getId())


#graph = buildGraph(keys,4)
#graph2 = buildGraph(keys,5)
#pprint(graph2.getVertices())                           #VERY IMPORTANT 

#bfs(graph2, graph2.getVertex('NAVAL'))

#traverse(graph2.getVertex('GLIDE'))    

print(" WELCOME TO WORD LADDER MAKER ")
size = raw_input("Please enter word length: ")
graph = buildGraph(keys,size)

word1 = raw_input("1st word: ")

